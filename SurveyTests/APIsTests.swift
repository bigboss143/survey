//
//  APIsTests.swift
//  SurveyTests
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import XCTest
@testable import Survey
class APIsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAccessTokenAPI() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        AuthAPIRequest.getRefreshToken("carlos@nimbl3.com",
                                       password: "antikera") { (status, authResponse) in
            XCTAssertTrue(status)
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
