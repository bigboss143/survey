//
//  SurveyVM.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 26/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

class SurveyVM: NSObject {
    var surveyList = [SurveyModel]()
    var success: ()->() = {}
    var showAlert : (_ title: String, _ message: String) -> () = {_,_ in}
    
    //MARK:-
    func onViewDidLoad() {
        self.loadSurveys()
    }
    //MARK:- Send for Approval
    func loadSurveys() {
        SurveyAPIRequest.getSurveyDetail(1, perPage: 2) { [weak self] (status, response) in
            if status {
                if let response = response {
                    self?.surveyList = response
                }
                DispatchQueue.main.async {
                    self?.success()
                }
            }else {
                DispatchQueue.main.async {
                    self?.showAlert("",MessageDefinitions.somethingWentWrongLocalString)
                }
            }
        }
    }
}
