//
//  UIImageView.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 04/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit
extension UIImageView {
    func downloadImageFrom(_ link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask(with: URL(string: link)!, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
