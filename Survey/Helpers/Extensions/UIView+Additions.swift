//
//  UIView+Additions.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 04/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

extension UIView {
    func roundView(_ cornerRadius: CGFloat? = nil,
                   borderWidth: CGFloat? = 0,
                   borderColor: UIColor? = .black) {
        self.layer.cornerRadius = cornerRadius ?? self.frame.size.height / 2
        self.layer.borderWidth = borderWidth ?? 0
        self.layer.borderColor = borderColor?.cgColor ?? UIColor.black.cgColor
        self.clipsToBounds = true
    }
}

