//
//  UIAlertAditions.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 04/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    class func showAlert(_ controller: UIViewController,
                         title: String?,
                         message: String?,
                         actions: [UIAlertAction]? = []) {
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if actions?.count == 0 {
            let okAction = UIAlertAction(title: MessageDefinitions.okString,
                                         style: .default,
                                         handler: nil)
            alertCont.addAction(okAction)
        }
        actions?.forEach({ (action) in
            alertCont.addAction(action)
        })
        controller.present(alertCont, animated: true, completion: nil)
    }
    class func showActionSheet(_ controller: UIViewController,
                               title: String?,
                               message: String?,
                               actions: [UIAlertAction]? = []) {
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        actions?.forEach({ (action) in
            alertCont.addAction(action)
        })
        let cancelAction = UIAlertAction(title: MessageDefinitions.cancelString,
                                         style: .destructive,
                                         handler: nil)
        alertCont.addAction(cancelAction)
        controller.present(alertCont, animated: true, completion: nil)
    }
}

