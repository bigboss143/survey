//
//  Constants.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

struct APIConstants {
    static let baseURL = "https://nimble-survey-api.herokuapp.com"
    static let accessTokenURL = "\(baseURL)/oauth/token"
    static let surveyURL = "\(baseURL)/surveys.json"
    
}

struct MessageDefinitions {
    static var okString =  "Ok"
    static var cancelString =  "Cancel"
    static var titleSurveys =  "SURVEYS"
    static var takeTheSurvey =  "Take the Survey"
    static var somethingWentWrongLocalString =  "Something went wrong"
}
