//
//  AuthAPIRequest.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import Foundation

class AuthAPIRequest: NSObject {
    class func getRefreshToken(_ userName:String, password:String, completionHandler:@escaping (_ status:Bool, _ response:AuthModel?)->Void) {
        NetworkManager.reuqest(AuthAPIRouter.accessToken(userName, password:password)) { (response) in
            switch response.result {
            case .success:
                guard let data = response.data else {
                    debugPrint("API : auth reponse data nil")
                    return
                }
                do {
                    let jsonDecoder = JSONDecoder()
                    let authResponse = try jsonDecoder.decode(AuthModel.self, from: data)
                    completionHandler(true,authResponse)
                }catch {
                    completionHandler(false,nil)
                }
            case .failure(_):
                completionHandler(false,nil)
            }
        }
    }
}
