//
//  AuthAPIRequest.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import Foundation

class SurveyAPIRequest: NSObject {
    class func getSurveyDetail(_ page:Int, perPage:Int, completionHandler:@escaping (_ status:Bool, _ response:Array<SurveyModel>?)->Void) {
        NetworkManager.reuqest(SurveyAPIRouter.surveDetails(page, perPage:perPage)) { (response) in
            debugPrint("Survey response \(response)")
            switch response.result {
            case .success:
                guard let data = response.data else {
                    debugPrint("API : Survey reponse data nil")
                    return
                }
                do {
                    let jsonDecoder = JSONDecoder()
                    let authResponse = try jsonDecoder.decode(Array<SurveyModel>.self, from: data)
                    completionHandler(true,authResponse)
                }catch {
                    completionHandler(false,nil)
                }
            case .failure(_):
                completionHandler(false,nil)
            }
        }
    }
}
