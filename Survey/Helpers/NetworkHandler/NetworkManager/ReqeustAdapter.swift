//
//  ReqeustAdapter.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 02/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import Alamofire
import Foundation

final class RequestInterceptor: Alamofire.RequestInterceptor {
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        guard urlRequest.url?.absoluteString.hasPrefix(APIConstants.baseURL) == true else {
            /// If the request does require authentication, we can directly return it as unmodified.
            return completion(.success(urlRequest))
        }
        var urlRequest = urlRequest
        /// Set the Authorization header value using the access token.
        urlRequest.setValue("Bearer " + Preferences.shared.accessToken, forHTTPHeaderField: "Authorization")
        completion(.success(urlRequest))
    }

    //MARK:- Retry requests
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 else {
            /// The request did not fail due to a 401 Unauthorized response.
            /// Return the original error and don't retry the request.
            return completion(.doNotRetryWithError(error))
        }
        AuthHandler.refreshAuthToken { (status) in
            if status {
                 completion(.retry)
            }else {
                 completion(.doNotRetryWithError(error))
            }
        }
    }
}
