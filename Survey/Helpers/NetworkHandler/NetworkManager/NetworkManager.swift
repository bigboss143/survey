//
//  NetworkManager.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {

    static let sessionManager = Alamofire.Session.default;
    
    class func reuqest(_ router: URLRequestConvertible, completionHandler: @escaping (AFDataResponse<Any>)-> Void) {
        sessionManager.request(router, interceptor: RequestInterceptor()).validate(statusCode: 200..<300).responseJSON { response in
            completionHandler(response)
        }
    }
}

