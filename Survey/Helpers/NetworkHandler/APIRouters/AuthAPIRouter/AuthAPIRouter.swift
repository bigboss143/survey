//
//  AuthAPIRouter.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import Alamofire

enum AuthAPIRouter: URLRequestConvertible {

    case accessToken(_ userName: String, password: String)
    
    var method: HTTPMethod {
        switch self {
        case .accessToken:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .accessToken:
            return APIConstants.accessTokenURL
        }
    }
    
    var params:Parameters? {
        switch self {
        case .accessToken(let userName, let password):
            return ["grant_type":"password",
                    "username":userName,
                    "password":password]
        }
    }

    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url:try path.asURL())
        urlRequest.httpMethod = method.rawValue
        switch self {
        case .accessToken:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params)
        }
        return urlRequest
    }
}
