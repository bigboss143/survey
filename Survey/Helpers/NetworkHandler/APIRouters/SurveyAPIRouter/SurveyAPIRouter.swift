//
//  AuthAPIRouter.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import Alamofire

enum SurveyAPIRouter: URLRequestConvertible {

    case surveDetails(_ page: Int, perPage: Int)
    
    var method: HTTPMethod {
        switch self {
        case .surveDetails:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .surveDetails:
            return APIConstants.surveyURL
        }
    }

    var params:Parameters? {
        return nil
    }

    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url:try path.asURL())
        urlRequest.httpMethod = method.rawValue
        switch self {
        case .surveDetails:
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params)
        }
        return urlRequest
    }
}
