//
//  AuthHandler.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 02/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

class AuthHandler: NSObject {
    
    class func refreshAuthToken(_ completionHandler: @escaping (_ status:Bool)->()) {
        AuthAPIRequest.getRefreshToken("carlos@nimbl3.com",
                                       password: "antikera") { (status, authResponse) in
            debugPrint("Auth response: \(String(describing: authResponse))")
            Preferences.shared.accessToken = authResponse?.accessToken ?? ""
            Preferences.shared.accessTokenExpiry = Int64(authResponse?.expiresIn ?? 0)
            completionHandler(status)
        }
    }
}
