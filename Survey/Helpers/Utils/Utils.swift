//
//  Utils.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 04/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

class Utils: NSObject {
    class func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    class func getCurrentSecond()->Int64 {
        return Int64(Date().timeIntervalSince1970)
    }
}
