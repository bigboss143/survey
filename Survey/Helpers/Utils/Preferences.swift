//
//  UserDefaults+Additions.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 02/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

 class Preferences {
      
      let defaults = UserDefaults.standard
      static let shared = Preferences()

    //MARK:- Access token
     var accessToken:String {
        get {
            return defaults.string(forKey: "accessToken") ?? "NA"
        }
        set {
            defaults.setValue(newValue, forKey: "accessToken")
            defaults.synchronize()
        }
    }
    //MARK:- Access token Expiry
     var accessTokenExpiry:Int64 {
        get {
            return Int64(defaults.integer(forKey: "accessTokenExpiry"))
        }
        set {
            defaults.setValue(newValue, forKey: "accessTokenExpiry")
            defaults.synchronize()
        }
    }
}
