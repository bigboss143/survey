//
//  AuthModel.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

//struct SurveyModel:Codable {
//    var data:[Survey]
//}
struct SurveyModel:Codable {
    let id:String
    let title: String
    let desc: String
    let coverImageURL: String
    
    private enum CodingKeys: String, CodingKey  {
        case id = "id"
        case title = "title"
        case desc = "description"
        case coverImageURL = "cover_image_url"
    }
}
extension SurveyModel: Equatable {
    static func == (lhs: SurveyModel, rhs: SurveyModel) -> Bool {
        return lhs.id == rhs.id
    }
}
