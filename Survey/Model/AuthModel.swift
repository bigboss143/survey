//
//  AuthModel.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 28/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//


struct AuthModel:Codable {
    let accessToken: String
    let tokenType: String
    let expiresIn: Int
    let createdAt: Int64
    
    private enum CodingKeys: String, CodingKey  {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case createdAt = "created_at"
    }
}
