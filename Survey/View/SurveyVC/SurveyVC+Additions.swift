//
//  SurveyVC+Additions.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 04/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//
import UIKit

extension SurveyVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return surveyVM.surveyList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SurveyRadioCell.self), for: indexPath) as! SurveyRadioCell
        if selectedSurvey == nil {
            selectedSurvey = surveyVM.surveyList[indexPath.row]
            loadCover(selectedSurvey)
        }
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let survey = surveyVM.surveyList[indexPath.row]
        if survey != selectedSurvey {
            selectedSurvey = survey
            loadCover(survey)
        }
    }
}
