//
//  SurveyRadioCell.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 04/03/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

class SurveyRadioCell: UITableViewCell {

    @IBOutlet weak var btnSurveyDetail: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnSurveyDetail.roundView(borderWidth: 2.0,
                                  borderColor: .white)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            btnSurveyDetail.backgroundColor = .white
        }else {
            btnSurveyDetail.backgroundColor = .clear
        }
    }

}
