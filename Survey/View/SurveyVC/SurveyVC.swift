//
//  ViewController.swift
//  Survey
//
//  Created by Muhammad Ahad Khan on 26/02/2020.
//  Copyright © 2020 Muhammad Ahad Khan. All rights reserved.
//

import UIKit

class SurveyVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var coverName: UILabel!
    @IBOutlet weak var coverDescription: UILabel!
    @IBOutlet weak var btnTakeTheSurvey: UIButton!
    @IBOutlet weak var coverImage: UIImageView!
    
    let surveyVM = SurveyVM()
    var selectedSurvey:SurveyModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero)
        setupVM()
        setupUI()
    }
    //MARK:- Setup view model
    func setupVM() {
        surveyVM.onViewDidLoad()
        surveyVM.showAlert = { [weak self] title, message in
            guard let _self = self else {return}
            UIAlertController.showAlert(_self, title: title, message: message)
        }
        surveyVM.success = { [weak self] in
            self?.tableView.reloadData()
        }
    }
    //MARK:- Setup UI
    func setupUI() {
        setupNavbarItems()
        self.title = MessageDefinitions.titleSurveys
        self.btnTakeTheSurvey.setTitle(MessageDefinitions.takeTheSurvey,
                                       for: .normal)
        self.btnTakeTheSurvey.roundView()
    }
    
    //MARK:- Take survey action
    @IBAction func takeSurveyAction(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: SurveyDetailVC.self)) as? SurveyDetailVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK:- Setup Navbar
    func setupNavbarItems() {
        let refreshButton =  UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadSurvey))
        self.navigationItem.leftBarButtonItem = refreshButton
    }
    @objc func reloadSurvey() {
        surveyVM.loadSurveys()
    }
    func loadCover(_ surveyModel: SurveyModel?) {
        if let surveyModel = surveyModel {
        self.coverName.text = surveyModel.title
        self.coverDescription.text = surveyModel.desc
        self.coverImage.downloadImageFrom(surveyModel.coverImageURL + "l", contentMode: UIView.ContentMode.scaleAspectFill)
        }
    }
}

